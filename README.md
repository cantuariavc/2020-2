# Plano da Disciplina - Aprendizado de Máquina (120642) [2nd semestre 2020]

## Professores
* Fabricio Ataides Braz
* Nilton Correia da Silva
* Pierre Guillou

## Período
2nd Semestre de 2020, do dia 01/02/2021 até 22/05/2021 ([calendario](http://saa.unb.br/images/stories/documentos/calendarios/graduacao/cal_atividades/cal_ati_2_2020.pdf))

## Turma
1

## Ementa
* Introdução a métodos de aprendizado de máquina que são comumente utilizados em aplicações de reconhecimento de padrões em sinais (texto, som e imagem). 
* Regressão. 
* Classificação 
* Aprendizado não supervisionado. 
* Máquinas de vetores de suporte. 
* Redes neurais artificiais. 

## Método

Independente do método de ensino, a construção de modelos de Inteligencia Artificial envolve conhecimentos, cuja apreensão demanda experimentos contínuos de exercício das suas técnicas e fundamentos. Várias abordagens servem ao própósito de motivar o aluno a buscar esse conhecimento. Neste semestre decidimos aplicar o método de aprendizado baseado em projeto. 

Uma das principais mudanças que aprendizado baseado em projeto traz é que o foco sai da **instrução**, em que o professor em sala de aula instrui o aluno sobre conceitos e ferramentas, para a **investigação**, em que o aluno é desafiado a pesquisar conceitos, técnicas e ferramentas para conseguir alcançar os objetivos do projeto que ele se comprometeu a desenvolver. A perspectiva do professor muda da **instrução**, para a **tutoria**, no que diz respeito ao ensino. A perspectiva do aluno muda de **passiva** para **ativa**, no que diz respeito ao aprendizado.

A disciplina prevê um total de 80 horas de formação, a serem distribuídas da seguinte maneira:

| Horas | Atividade                          | Natureza |
|-------|------------------------------|---------|
| 6   | Acolhimento e nivelamento              | Mista |
| 20   | Ensino (encontros online)                                | Sincrona |
| 18   | Videos do curso                                 | Assincrona |
| 20   | Atividades em Grupo                    | Assincrona |
| 12    | Tutorado                           | Mista |
| 4    | Seminários                           | Sincrona |

Serão constituídos grupos em que o aluno, apoioado por até 3 (três) tutores (professores e monitores), percorrerá uma trilha de aprendizagem voltada para a construção de dashboards.

No que diz repeito a abordagem técnica para aprendizado de máquina, daremos preferência aos **modelos de aprendizagem profunda** (*deep learning*). Em razao disso, o tópico **redes neurais artificiais** será base para o ensino de modelagem supervisionada (classifição/regressão). Ao inves de modelos de maquina de suporte, abordaremos árvores.

## Ferramentas & Materiais
* [Teams](https://teams.microsoft.com/l/team/19%3acd6a018f90504eee9682287bc21fa04c%40thread.tacv2/conversations?groupId=7ceb1164-4fb9-43bb-8949-e79e2f0dabfd&tenantId=ec359ba1-630b-4d2b-b833-c8e6d48f8059) - Comunicação e trabalho colaborativo;
* [Trello](https://trello.com/b/9utfZSs4/project-based-learning-am-2nd-sem-2020) - gerência do projeto;
* Python - Linguagem de programação;
* [Gitlab](https://gitlab.com/ensino_unb/am/2020-2) - Reposotório de código e coloboração;
* [Forum de Discussão](https://forum.ailab.unb.br)

## Avaliação

Para que o aluno seja aprovado na disciplina ele deve possuir desempenho superior ou igual a 50, correspondente a menção MM. Além disso, seu comparecimento deve ser superior ou igual a 75% dos eventos estabelecidos pela disciplina. 

### Desempenho

A avaliação de desempenho é resultado da avaliação pelos professores do resultado do grupo, dado o projeto, juntamente com a avaliação individual do aluno pelos membros do grupo.

* AGP: avaliação do grupo pelos professores. Esta avaliação acontece de acordo com os marcos estabelecidos no template de projeto do Trello compatilhado. No dia da apresentação será sorteado um membro do grupo que deverá ter a capacidade de responder pelo projeto.

* AIG: avaliação individual pelos membros do grupo.

![equation](http://www.sciweavers.org/upload/Tex2Img_1611855693/render.png)

Serão dois encontros avaliativos, observando os seguintes apectos do projeto:
1. Design e Apresentação (peso 1)
2. Desenvolvimento, Testes e Apresentação (peso 2)

As fases e datas encontram-se detalhadas no Trello. Aqui estão as datas principais:

- Antes do 16/03: Criação dos grupos e aprovação das ideias de projetos
- 16/03 - DEADLINE 1 (Design e Apresentação, peso 1) - Apresente o seu projeto ao professor por avaliação.
- 18/05 - DEADLINE 2 (Desenvolvimento, Testes e Apresentação, peso 2) - Apresente o seu produto final ao professor.

### Comparecimento

As atividades síncronas serão consideradas para contabilizar a presença.

## Dinâmica no Grupo
Os grupos serão formados com a quantidade 5 alunos. Em cada iteração, ou seja, períodos entre os encontros avaliativos, deverão ser designados no grupo o `líder`, responsável por conduzir o time na execução das metas do período, além de reportar aos professores desvios e problemas de seus membros.

### Equilíbrio
Para que o grupo não seja prejudicado por eventuais desvios de seus membros, os alunos que não alcancarem nota igual ou superior a 5 na AIG serão desprezados do sorteio para as apresentações.

### Evasão
Grupos com menos de 5 alunos, terão seus membros distribuidos em outros grupos.

## Referências Bibliográficas

### Básica

* Kevin Patrick Murphy. Machine Learning: a Probabilistic Perspective Editor MIT press. 2012. Cambridge, MA.

* Chris Bishop. Pattern Recognition and Machine Learning. Editor Springer. 2006. New York.

* Ian Goodfellow, Yoshua Bengio, Aaron Courville. Deep Learning Editor MIT press. 2017. Cambridge, MA.

* [Yaser S. Abu-Mostafa, Malik Magdon-Ismail, Hsuan-Tien Lin. Learning from Data - a Short Course.](https://work.caltech.edu/telecourse.html). AML Book. 2012. Pasadena, CA. 

* Tom M. Mitchell. Machine Learning Editor. McGraw-Hill. 1997 

* David Barber. Bayesian Reasoning and Machine Learning. Cambridge University Press. 2012. Cambridge, UK. 

* Carl Edward Rasmussen, Christopher K. I. Williams. Gaussian Processes For Machine Learning Editor MIT press. 2006. Cambridge, MA 

* [Andrew Ng Local. Machine Learning Video Lectures](https://www.coursera.org/learn/machine-learning). Stanford, CA. 2014

### Complementar

* [Jeremy Howard and Sylvain Gugge. FastBook](https://github.com/fastai/fastbook)
* [AI Lab Forum](https://forum.ailab.unb.br)
* [Blog](https://medium.com/@pierre_guillou)


